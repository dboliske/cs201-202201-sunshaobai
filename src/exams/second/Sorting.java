/** 
 * Author: Shaobai Sun 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 29, 2022
 * Section: section 3
 * Final Exam:
 * Question Four: Sorting
 * Write a Java program that implements the Selection Sort Algorithm for an Array (or ArrayList) of Strings and prints the sorted results.
 * {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"}
 */

package exams.second;

import java.util.ArrayList;
import java.util.Arrays;

public class Sorting {

	// selection sort implemented with ArrayList
	public static ArrayList<String> sort(ArrayList<String> list) {
		for (int i = 0; i < list.size() - 1; i++) {
			int min = i;
			for (int j = i + 1; j < list.size(); j++) {
				if (list.get(j).compareTo(list.get(min)) < 0) {
					min = j;
				}
			}
			
			if (min != i) {
				String tmp = list.get(i);
				list.set(i, list.get(min));
				list.set(min, tmp);
			}
		}
		return list;
	}
	
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>(Arrays.asList("speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"));
		list = sort(list);
		System.out.println("sort result:");
		System.out.println(list.toString());
	}

}
