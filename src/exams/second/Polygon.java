/** 
 * Author: Shaobai Sun 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 29, 2022
 * Section: section 3
 * Final Exam:
 * Question Two: Abstract Classes
 * Write the Java classes based on the following UML diagram.
 * Include all methods shown (you do not need to include any additional ones).
 * Notes: - The radius, height, and width should always be positive and can default to 1.
 * - The area of a circle is Math.PI * radius * radius and the perimeter is 2.0 * Math.PI * radius.
 * - The area of a rectangle is height * width and the perimeter is 2.0 * (height + width).
 */

package exams.second;

public abstract class Polygon {

	public String name;
	
	public Polygon() {
		name = "Polygon";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return "Name: " + name;
	}
	
	public abstract double area();
	public abstract double perimeter();
}
