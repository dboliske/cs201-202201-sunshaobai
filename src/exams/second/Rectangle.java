/** 
 * Author: Shaobai Sun 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 29, 2022
 * Section: section 3
 * Final Exam:
 * Question Two: Abstract Classes
 * Write the Java classes based on the following UML diagram.
 * Include all methods shown (you do not need to include any additional ones).
 * Notes: - The radius, height, and width should always be positive and can default to 1.
 * - The area of a circle is Math.PI * radius * radius and the perimeter is 2.0 * Math.PI * radius.
 * - The area of a rectangle is height * width and the perimeter is 2.0 * (height + width).
 */

package exams.second;

public class Rectangle extends Polygon {
	
	private double width;
	private double height;
	
	public Rectangle() {
		super();
		width = 1.0;
		height = 1.0;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		if (width >= 0.0) {
			this.width = width;
		}
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		if (height >= 0) {
			this.height = height;
		}
	}
	
	public String toString() {
		return "Name: " + name + ", width: " + width + ", height: " + height +
				", area: " + area() + ", perimeter: " + perimeter();
	}

	@Override
	public double area() {
		return height * width;
	}

	@Override
	public double perimeter() {
		return 2.0 * (height + width);
	}

}
