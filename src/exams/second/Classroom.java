/** 
 * Author: Shaobai Sun 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 29, 2022
 * Section: section 3
 * Final Exam:
 * Question One: Inheritance/Polymorphism
 * Write the Java classes based on the following UML diagram. 
 * Include all methods shown (you do not need to include any additional ones).
 * Notes: - seats must be positive
 */

package exams.second;

public class Classroom {

	protected String building;
	protected String roomNumber;
	private int seats;
	
	public Classroom() {
		building = "Building_A";
		roomNumber = "01";
		seats = 0;
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		if (seats >= 0) {
			this.seats = seats;
		}
	}
	
	public String toString() {
		return building + " room " + roomNumber + " has " + seats + " seat(s).";
	}
}
