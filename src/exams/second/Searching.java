/** 
 * Author: Shaobai Sun 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 29, 2022
 * Section: section 3
 * Final Exam:
 * Question Five: Searching
 * Write a Java program that implement the Jump Search Algorithm recursively for an Array
 * (or ArrayList) of numbers and allows the user to search for a value,
 * printing the position of where that value is found. If the value is not present, it should print -1.
 * {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142}
 */

package exams.second;

import java.util.Scanner;

public class Searching {
	
	public static int search(double[] arr, double value) {
	    int step = (int) Math.sqrt(arr.length);
	    return jumpSearch(arr, value, step, 0);
	}

	// Jump Search Algorithm recursively for an Array
	public static int jumpSearch(double[] arr, double value, int step, int start) {
	    if (start + step - 1 < arr.length && value > arr[start + step - 1]) {
	    	start += step;
	    	// call to jumpSearch recursively
	        return jumpSearch(arr, value, step, start);
	    } else {
	    	// find accurate position of value using sequential search
	        for (int i = start; i <= start + step - 1 && i < arr.length; i++) {
	            if (value == arr[i]) {
	                return i;
	            }
	        }
	    }
	    
	    return -1;  // Not found
	}

	public static void main(String[] args) {
		double[] arr = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
        Scanner input = new Scanner(System.in);
        boolean done = false;
        do {
        	System.out.println("Please enter a number to search: ");
        	double number = 0.0;
        	try {
        		number = Double.parseDouble(input.nextLine());
        		int position = search(arr, number);
        		System.out.println(position);
        		done = true;
        	} catch (Exception e) {
				System.out.print("Error: Not a number! ");
			}
        } while(!done);
        
        input.close();
	}

}
