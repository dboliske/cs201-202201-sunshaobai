/** 
 * Author: Shaobai Sun 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 29, 2022
 * Section: section 3
 * Final Exam:
 * Question One: Inheritance/Polymorphism
 * Write the Java classes based on the following UML diagram. 
 * Include all methods shown (you do not need to include any additional ones).
 * Notes: - seats must be positive
 */

package exams.second;

public class ComputerLab extends Classroom {

	private boolean computers;
	
	public ComputerLab() {
		computers = false;
	}

	public boolean hasComputers() {
		return computers;
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	public String toString() {
		if (computers) {
			return super.toString() + " It is a computer lab which has computers.";
		} else {
			return super.toString() + " It is a computer lab which has no computer.";
		}
	}
	
}
