/** 
 * Author: Shaobai Sun 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 29, 2022
 * Section: section 3
 * Final Exam:
 * Question Three: ArrayLists
 * Write a Java program that prompts the user to enter a sequence of numbers, storing them in an ArrayList,
 * and continues prompting the user for numbers until they enter "Done". 
 * When they have finished entering numbers,your program should return the minimum and maximum values entered.
 */

package exams.second;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class ArrayLists {
	
	public static void menu(Scanner input) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		String prompts = "Please enter numbers separated by one or more spaces, or ',' \nPlease enter 'Done' to finish entering numbers.";
		boolean done = false;
		do {
			System.out.println(prompts);
			String line = input.nextLine();
			if (line.equals("Done")) {
				showFinalResult(list);
				done = true;
			} else {
				store(list, line);
			}
		} while (!done);
	}
	
	// sort the list by natural order and print minimum and maximum values as final result
	public static void showFinalResult(ArrayList<Integer> list) {
		if (list.size() > 0) {  // list is not empty
			list.sort(Comparator.naturalOrder());
			System.out.println("min: " + list.get(0) + ", max: " + list.get(list.size() - 1));
		}
	}
	
	// store the input numbers to a list
	public static ArrayList<Integer> store(ArrayList<Integer> list, String line) {
		// use regular expression to split the line with one or more spaces, or ','
		String values[] = line.split("\s+|,");
		ArrayList<Integer> tmpList = new ArrayList<Integer>();
		for (int i = 0; i < values.length; i++) {
			int value = 0;
			try {
			    value = Integer.parseInt(values[i]);
			    tmpList.add(value);
			} catch (Exception e) {
				System.out.println("Input format error!");
				break;
			}
		}
		
		//  No input format error, tmpList and values array have the same size
		if (tmpList.size() == values.length) {
			list.addAll(tmpList);  // append the tmpList to original list
		}
		for (Integer i: list) {
			System.out.println(i);
		}
		return list;
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		menu(input);
		System.out.print("Goodbye...");
		input.close();
	}

}
