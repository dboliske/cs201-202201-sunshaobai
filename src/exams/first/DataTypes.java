/* 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Feb. 25, 2022
 * Section: section 3
 * Midterm Exam
 * Question One: Data Types
 * Write a program that prompts the user for an integer, add 65 to it, 
 * convert the result to a character and print that character to the console.
 */

package exams.first;

import java.util.Scanner;

public class DataTypes {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);  // create a scanner
		String str_value = "";
		int int_value = 0;
		boolean done = false;
		do {
			System.out.println("Please enter an integer: ");
			// deal with the invalid input to avoid program crash
			try {
				str_value = input.nextLine();
				int_value = Integer.parseInt(str_value);
				done = true;
			} catch (Exception e) {
				System.out.println("Input Error! " + str_value + " is not an integer, please enter again!");
			}
		} while(!done);
		
		input.close();
		
		int_value += 65;  // add 65 to it
		char char_value = (char)int_value;  // convert the result to a character
		System.out.println(char_value);	// print the character to the console
	}

}
