/* 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Feb. 25, 2022
 * Section: section 3
 * Midterm Exam
 * Question Three: Repetition
 * Write a program that prompts the user for an integer and then prints out a Triangle of that height and width. 
 * For example, if the user enters 3, then your program should print the following:
 *		* * *
 *  	  * *
 *  	    *
 */

package exams.first;

import java.util.Scanner;

public class Repetition {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);  // create a scanner
		String str_value = "";
		int int_value = 0;
		boolean done = false;
		do {
			System.out.println("Please enter an integer: ");
			// deal with the invalid input to avoid program crash
			try {
				str_value = input.nextLine();
				int_value = Integer.parseInt(str_value);
				done = true;
			} catch (Exception e) {
				System.out.println("Input Error! " + str_value + " is not an integer, please enter again!");
			}
		} while(!done);
		
		input.close();
		
		for(int row = 0; row < int_value; row++) {
			for(int col = 0; col < int_value; col++) {
				if (row > col) {  // print 2 space characters
				    System.out.print("  ");
				} else {  // print 1 space characters and 1 * characters
					System.out.print(" *");
				}
			}
			System.out.println();  // Star a new line
		}

	}

}
