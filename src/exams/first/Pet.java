/* 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Feb. 25, 2022
 * Section: section 3
 * Midterm Exam
 * Question Five: Objects
 * Write a Java class based on the following UML diagram. Include all standard methods, such as constructors,
 * mutators, accessors, toString, and equals. Additionally, implement any other methods shown in the diagram.
 * NOTES:
 * age needs to be positive.
 * You do not need to write an application class.
 */

package exams.first;

public class Pet {
	private String name;
	private int age;
	
	// default constructor
	public Pet() {
		name = "xxx";
		age = 0;
	}
	
	// non-default constructor
	public Pet(String name, int age) {
		this.name = name;
		if (validAge(age)) {  // use validAge method to validate the age
			this.age = age;
		} else {
			this.age = 0;  // set to default
		}
	}
	
	// accessor methods for name
	public void setName(String name) {
		this.name = name;
	}
	
	// accessor methods for age
	public void setAge(int age) {
		if (validAge(age)) {
			this.age = age;
		} else {
			System.out.println("The age is invalid, set to default value 0 !");
		    this.age = 0;
		}
	}
	
	public boolean validAge(int age) {
		if (age >= 0) {
			return true;
		}
		
		return false;
	}
	
	// mutator method for name
	public String getName() {
		return name;
	}
	
	// mutator method for age
	public int getAge() {
		return age;
	}
	
	// equals method
	@Override
	public boolean equals(Object obj) {
		// check the obj is an instance of Pet or not, if not return false
		if (!(obj instanceof Pet)) {
			return false;
		}
		
		// if the obj is an instance of Pet, convert it to instance of Pet
		Pet pet = (Pet)obj;
		if (!this.name.equals(pet.getName())) {
			return false;
		} else if (this.age != pet.getAge()) {
			return false;
		}
		
		return true;
	}
	
	// toString method
	@Override
	public String toString() {
		return name + " is " + age + " years old.";
	}

}
