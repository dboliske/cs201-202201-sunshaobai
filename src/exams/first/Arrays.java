/* 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Feb. 25, 2022
 * Section: section 3
 * Midterm Exam
 * Question Four: Arrays
 * Write a program that prompts the user for 5 words and prints out any word that appears more than once.
 * NOTE:
 * The words should be an exact match, i.e. it should be case sensitive.
 * Your program must use an array.
 * Do not sort the data or use an ArrayLists.
 */

package exams.first;

import java.util.Scanner;

public class Arrays {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		final int SIZE = 5;
		String[] words = new String[SIZE];
		System.out.println("Please enter 5 words: ");
		
		for(int i = 0; i < SIZE; i++) {
			System.out.println("The [" + (i + 1) + "] word: ");
			words[i] = input.nextLine();
		}
		
		input.close();
		
		// mark the element is appears more than once
	    boolean[] repeat_flags = new boolean[SIZE];
	    // store the repeat count
	    int[] repeat_counts = new int[SIZE];
		for(int i = 0; i < words.length; i++) {
			for(int j = i + 1; j < words.length; j++) {
				if (words[i].equals(words[j])) {
					repeat_counts[i] += 1;
					if (!repeat_flags[j]) {
						repeat_flags[j] = true;
					}
				}
			}
		}

		int count = 0;
		// new array to store the word that appears more than once
		String[] new_words = new String[SIZE];
		for(int i = 0; i < words.length; i++) {
			if (!repeat_flags[i] && repeat_counts[i] > 0) {
				new_words[i] = words[i];
				count++;
			}
		}
		
		if (count == 0) {
			System.out.println("No word that appears more than once!");
		} else if (count > 0) {
			// trim new array down
			String[] smaller = new String[count];
			for(int i = 0; i < smaller.length; i++) {
				smaller[i] = new_words[i];
			}
			new_words = smaller;
			smaller = null;
			
			System.out.println("Below word(s) appear(s) more than once:");
			for (int i = 0; i < new_words.length; i++) {
				System.out.println(new_words[i]);
			}
		}
	}

}
