/* 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Feb. 25, 2022
 * Section: section 3
 * Midterm Exam
 * Question Two: Selection
 * Write a program that prompts the user for an integer. If the integer is divisible by 2 print out "foo", 
 * and if the integer is divisible by 3 print out "bar". 
 * If the integer is divisible by both, your program should print out "foobar" 
 * and if the integer is not divisible by either, then your program should not print out anything.
 */

package exams.first;

import java.util.Scanner;

public class Selection {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);  // create a scanner
		String str_value = "";
		int int_value = 0;
		boolean done = false;
		do {
			System.out.println("Please enter an integer: ");
			// deal with the invalid input to avoid program crash
			try {
				str_value = input.nextLine();
				int_value = Integer.parseInt(str_value);
				done = true;
			} catch (Exception e) {
				System.out.println("Input Error! " + str_value + " is not an integer, please enter again!");
			}
		} while(!done);
		
		input.close();
		
		if (int_value % 2 == 0 && int_value % 3 != 0) {
			// the integer is divisible by 2 (only 2)
			System.out.println("foo");
		} else if (int_value % 2 != 0 && int_value % 3 == 0) {
			// the integer is divisible by 3 (only 3)
			System.out.println("bar");
		} else if (int_value % 2 == 0 && int_value % 3 == 0) {
			// the integer is divisible by both (2 and 3)
			System.out.println("foobar");
		}
	}

}
