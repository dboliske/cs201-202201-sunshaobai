/** 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 17, 2022
 * Section: section 3
 * Final Project:
 * A program that models a general store and allows users to add stock to the store,
 * search for available stock, and sell items.
 * Class name: GeneralStoreApplication
 * Description: This class is the application main class, and it can interact with user. 
 */

package project;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Pattern;

public class GeneralStoreApplication {
	// global StoreStock object
	public static StoreStock storeStock;
	
	// read data from file and store to a list
	public static ArrayList<Item> readFile(String filename) {
		ArrayList<Item> items = new ArrayList<Item>();
		
		try {
			File f = new File(filename);
			Scanner in = new Scanner(f);
			// end-of-file loop
			while (in.hasNextLine()) {
				String line = in.nextLine();
				String[] values = line.split(",");
				Item item = null;
				int splitsCount = calculateSplitsCount(line, ',');
				if (splitsCount == 1) {
					item = new ShelvedItem(values[0],
							Double.parseDouble(values[1]));
				} else if (splitsCount == 2) {
					if (checkDateFormat(values[2])) {
						item = new ProduceItem(values[0],
								Double.parseDouble(values[1]),
								values[2]);
					} else {
						item = new AgeRestrictedItem(values[0],
								Double.parseDouble(values[1]),
								Integer.parseInt(values[2]));
					}
				} else {
					System.out.println("Data format error!");
				}
				items.add(item);
			}
			in.close();
			
		} catch (Exception e) {
			System.out.println("Read file error!");
			System.out.println(e.getMessage());
		}
		
		return items;
	}
	
	// calculate the count of splits of a string
	public static int calculateSplitsCount(String str, char split) {
		int count = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == split) {
				count++;
			}
		}
		return count;
	}
	
	// method to check the date format is valid or not
	public static boolean checkDateFormat(String date) {
		String datePattern = "([1-9]|[0][1-9]|[1][0-2])(-|/)"
				+ "([1-9]|[0][1-9]|[12][0-9]|[3][01])(-|/)"
				+ "([0-9]+)";
		if (Pattern.matches(datePattern, date)) {
			return true;
		}
		
		return false;
	}
	
	// save item data to a file
	public static void saveFile(String filename) {
		try {
			FileWriter writer = new FileWriter(filename);
			ArrayList<Item> stockList = storeStock.getStocksList();
			for (Item item : stockList) {
				writer.write(item.toCSV() + "\n");
				writer.flush();
			}
			
			writer.close();
		} catch (Exception e) {
			System.out.println("Error saving to file.");
		}
	}
	
	// the main menu method
	public static void menu(Scanner input) {
		boolean done = false;
		ArrayList<String> options = new ArrayList<String>(Arrays.asList("Create a new items", "Sell items", "Search items", "Modify items", "Delete items", "Show items stock", "Exit program"));
		
		do {
			System.out.println("Please select an opration below:");
			for(int i = 0; i < options.size(); i++) {  // Print all prompt options in this loop
				System.out.println((i + 1) + ". " + options.get(i));
			}
			System.out.print("Your choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1":  // Create a new items
					createNewItems(input);
					break;
				case "2":  // Sell items
					sellItems(input);
					break;
				case "3":  // Search items
					searchItems(input);
					break;
				case "4":  // Modify items
					modifyItems(input);
					break;
				case "5":  // Delete items
					deleteItems(input);
					break;
				case "6":  // Show items stock
					showItemsStock();
					break;
				case "7":  // Exit program
					done = true;
					break;
				default:
					System.out.println("Invalid input, please input angain!");
			}
		} while (!done);
	}
	
	// create new items
	public static void createNewItems(Scanner input) {
		boolean done = false;
		ArrayList<String> options = new ArrayList<String>(Arrays.asList("a. Produce Item", "b. Shelved Item", "c. Age Restricted Item", "d. Return to main menu"));
		
		do {
			System.out.println("Please selcet item type you will create:");
			for(int i = 0; i < options.size(); i++) {  // Print all prompt options in this loop
				System.out.println(options.get(i));
			}
			System.out.print("Your choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "a":  // Create Produce Item
					createProduceItem(input);
					break;
				case "b":  // Create Shelved Item
					createShelvedItem(input);
					break;
				case "c":  // Create Age Restricted Item
					createAgeRestrictedItem(input);
				    break;
				case "d":
					done = true;
					break;
				default:
					System.out.println("Invalid input, please input angain!");
			}
		} while (!done);
	}
	
	// create produce item
	public static void createProduceItem(Scanner input) {
		Item item;
		System.out.print("Prodece Item Name:");
		String name = input.nextLine();
		System.out.print("Prodece Item Price:");
		double price = 0.0;
		try {
			price = Double.parseDouble(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid Price! Return to item type menu...");
			return;
		}
		
		System.out.print("Prodece Item Date(MM/DD/YYYY):");
		String date = input.nextLine();
		if (!checkDateFormat(date)) {
			System.out.println("Invalid Date! Return to item type menu...");
			return;
		}
		
		item = storeStock.createItem(name, price, date);
		System.out.println("Create {" + item.toString() + "} success...");
		storeStock.addItem(item);
	}
	
	// create shelved item
	public static void createShelvedItem(Scanner input) {
		Item item;
		System.out.print("Shelved Item Name:");
		String name = input.nextLine();
		System.out.print("Shelved Item Price:");
		double price = 0.0;
		
		try {
			price = Double.parseDouble(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid Price! Return to item type menu...");
			return;
		}
		
		item = storeStock.createItem(name, price);
		System.out.println("Create {" + item.toString() + "} success...");
		storeStock.addItem(item);	
	}
	
	// create age restricted item
	public static void createAgeRestrictedItem(Scanner input) {
		Item item;
		System.out.print("Age restricted Item Name:");
		String name = input.nextLine();
		System.out.print("Age restricted Item Price:");
		double price = 0.0;
		try {
			price = Double.parseDouble(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid Price! Return to item type menu...");
			return;
		}
		
		System.out.print("Age restricted Item Age:");
		int restrictAge = 0;
		try {
			restrictAge = Integer.parseInt(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid restrict age! Return to item type menu...");
			return;
		}
		
		item = storeStock.createItem(name, price, restrictAge);
		System.out.println("Create {" + item.toString() + "} success...");
		storeStock.addItem(item);
	}
	
	// sell items
	public static void sellItems(Scanner input) {
		System.out.print("Sell Item Name:");
		String name = input.nextLine();
		System.out.print("Sell Item count:");
		int count = 0;
		
		try {
			count = Integer.parseInt(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid count! Return to main menu...");
			return;
		}
		
		System.out.print("Add to cart? (y/n):");
		String result = input.nextLine();
		if (result.toLowerCase().equals("y") || result.toLowerCase().equals("yes")) {
			if (!storeStock.addToCart(name, count)) {
				return;
			}
		} else if (result.toLowerCase().equals("n") || result.toLowerCase().equals("no")) {
			System.out.println("Not add to cart, return to main menu...");
			return;
		} else {
			System.out.println("Invalid input, return to main menu...");
			return;
		}
		
		System.out.print("Please input money($):");
		double dollar = 0.0;
		try {
			dollar = Double.parseDouble(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid Price! Return to main menu...");
			return;
		}
		
		System.out.print("Confirm to checkout? (y/n):");
		result = input.nextLine();
		if (result.toLowerCase().equals("y") || result.toLowerCase().equals("yes")) {
			if (!storeStock.checkout(dollar)) {
				return;
			}
		} else if (result.toLowerCase().equals("n") || result.toLowerCase().equals("no")) {
			System.out.println("Not add to cart, return to main menu...");
			return;
		}
		
		System.out.println("Invalid input, return to main menu...");
	}
	
	// search items
	public static void searchItems(Scanner input) {
		System.out.print("Search Name: ");
		String name = input.nextLine();
		ArrayList<Item> resultList =  storeStock.searchItem(name);
		if (resultList.size() > 0) {
			System.out.println("Search sueecss, find " + resultList.size() + " " + name + "(s):");
			for (Item item : resultList) {
				System.out.println(item.toString());
			}
		} else {
			System.out.println("Search failed! Invalid name! Return to main menu...");
		}
	}
	
	// modify items
	public static void modifyItems(Scanner input) {
		System.out.println("Please input name of items:");
		System.out.print("Name: ");
		String name = input.nextLine();
		String type = storeStock.getItemType(name);
		if (type.equals("ShelvedItem")) {
			modifyShelvedItem(name, input);
		} else if (type.equals("ProduceItem")) {
			modifyProduceItem(name, input);
		}  else if (type.equals("AgeRestrictedItem")) {
			modifyAgeRestrictedItem(name, input);
		} else {
			System.out.println("Invalid name! Return to main menu...");
		}
	}
	
	// modify shelved items
	public static void modifyShelvedItem(String name, Scanner input) {
		System.out.print("New name: ");
		String newName = input.nextLine();
		System.out.print("New price: ");
		double newPrice = 0.0;
		try {
			newPrice = Double.parseDouble(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid newPrice! Return to main menu...");
			return;
		}
		storeStock.modifyItem(name, newName, newPrice);
		System.out.println("Modify " + name + " success...");
	}
	
	// modify produce items
	public static void modifyProduceItem(String name, Scanner input) {
		System.out.print("New name: ");
		String newName = input.nextLine();
		System.out.print("New price: ");
		double newPrice = 0.0;
		try {
			newPrice = Double.parseDouble(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid newPrice! Return to main menu...");
			return;
		}
		
		System.out.print("New date(MM/DD/YYYY):");
		String newDate = input.nextLine();
		if (!checkDateFormat(newDate)) {
			System.out.println("Invalid Date! Return to item type menu...");
			return;
		}
		storeStock.modifyItem(name, newName, newPrice, newDate);
		System.out.println("Modify " + name + " success...");
	}
	
	// modify age restricted items
	public static void modifyAgeRestrictedItem(String name, Scanner input) {
		System.out.print("New name: ");
		String newName = input.nextLine();
		System.out.print("New price: ");
		double newPrice = 0.0;
		try {
			newPrice = Double.parseDouble(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid newPrice! Return to main menu...");
			return;
		}
		
		System.out.print("New restrict age: ");
		int newRestrictAge = 0;
		try {
			newRestrictAge = Integer.parseInt(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid restrict age! Return to item type menu...");
			return;
		}
		
		storeStock.modifyItem(name, newName, newPrice, newRestrictAge);
		System.out.println("Modify " + name + " success...");
	}
	
	// delete items
	public static void deleteItems(Scanner input) {
		System.out.print("Delete Name:");
		String name = input.nextLine();
		System.out.print("Delete count:");
		int count = 0;
		try {
			count = Integer.parseInt(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid count! Return to main menu...");
			return;
		}
		if (storeStock.removeItems(name, count)) {
			System.out.println(count + " " + name + "(s) was deleted success...");
		}
	}
	
	// show items stock
	public static void showItemsStock() {
		HashMap<String, Integer> stocksMap = storeStock.getItemsStockMap();
		System.out.println("Item stocks:");
		for (String k : stocksMap.keySet()) {
			System.out.println(k + ": " + stocksMap.get(k));
		}
	}

	public static void main(String[] args) {
		String filePath = "./src/project/stock.csv";
		ArrayList<Item> items = readFile(filePath);
		storeStock = new StoreStock(items);

		Scanner input = new Scanner(System.in);
		menu(input);
		saveFile(filePath);
		input.close();
		System.out.println("Goodbye!");
	}

}
