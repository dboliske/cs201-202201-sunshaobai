/** 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 17, 2022
 * Section: section 3
 * Final Project:
 * A program that models a general store and allows users to add stock to the store,
 * search for available stock, and sell items.
 * Class name: ProduceItem
 * Description: This class is the sub class of Item, which encapsulates all the properties and method of Produced Item.
 */

package project;

import java.util.regex.Pattern;

public class ProduceItem extends Item {
	private String date;
	
	public ProduceItem() {
		super();
		date = "01/01/2000";
	}
	
	public ProduceItem(String name, double price, String date) {
		super(name, price);
		this.date = "01/01/2000";
		setDate(date);
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		String datePattern = "([1-9]|[0][1-9]|[1][0-2])(-|/)"
				+ "([1-9]|[0][1-9]|[12][0-9]|[3][01])(-|/)"
				+ "([0-9]+)";
		if (Pattern.matches(datePattern, date)) {
			this.date = date;
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof ProduceItem)) {
			return false;
		}
		
		ProduceItem prdItm = (ProduceItem) obj;
		if (!(this.getDate().equals(prdItm.getDate()))) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return "ProduceItem: Name is " + name + ", price is $" +
	            price + ", date is " + date;
	}
	
	// Method to generate csv format of name, price and date
	@Override
	public String toCSV() {
		return name + "," + price + "," + date;
	}

}
