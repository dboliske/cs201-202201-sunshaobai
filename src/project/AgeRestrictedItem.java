/** 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 17, 2022
 * Section: section 3
 * Final Project:
 * A program that models a general store and allows users to add stock to the store,
 * search for available stock, and sell items.
 * Class name: ShelvedItem
 * Description: This class is the sub class of Item, which encapsulates all the properties and method of AgeRestricted Item.
 */

package project;

public class AgeRestrictedItem extends Item {

	private int restrictAge;
	
	public AgeRestrictedItem() {
		super();
		restrictAge = 0;
	}
	
	public AgeRestrictedItem(String name, double price, int restrictAge) {
		super(name, price);
		this.restrictAge = 0;  // Set a default value
		setRestrictAge(restrictAge);
	}

	public int getRestrictAge() {
		return restrictAge;
	}

	public void setRestrictAge(int restrictAge) {
		if (restrictAge >= 0) {
			this.restrictAge = restrictAge;
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof AgeRestrictedItem)) {
			return false;
		}
		
		AgeRestrictedItem ageResItm = (AgeRestrictedItem) obj;
		if (this.getRestrictAge() != ageResItm.getRestrictAge()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return "AgeRestrictedItem: Name is " + name + ", price is $" +
	            price + ", restricted age is " + restrictAge;
	}
	
	// Method to generate csv format of name, price and restrict age
	@Override
	public String toCSV() {
		return name + "," + price + "," + restrictAge;
	}

}
