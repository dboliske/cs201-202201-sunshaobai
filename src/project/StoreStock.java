/** 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 17, 2022
 * Section: section 3
 * Final Project:
 * A program that models a general store and allows users to add stock to the store,
 * search for available stock, and sell items.
 * Class name: StoreStock
 * Description: This class is used for manage all kinds of Items and encapsulates various interfaces of user��s action.
 * The stocksList is used to store the all the items of the store,
 * and cartList is used to store the items in in user��s cart that user want to buy.
 */

package project;

import java.util.ArrayList;
import java.util.HashMap;

public class StoreStock {

	// list to store stocks
	private ArrayList<Item> stocksList;
	// list to store items in the cart
	private ArrayList<Item> cartList;
	
	public StoreStock() {
		stocksList = new ArrayList<Item>();
		cartList = new ArrayList<Item>();
	}
	
	public StoreStock(ArrayList<Item> stocksList) {
		this.stocksList = new ArrayList<Item>(stocksList.size());
		for (Item item : stocksList) {
			this.stocksList.add(item);
		}
		cartList = new ArrayList<Item>();
	}
	
	public StoreStock(ArrayList<Item> stocksList, ArrayList<Item> cartList) {
		this.stocksList = new ArrayList<Item>(stocksList.size());
		this.cartList = new ArrayList<Item>(cartList.size());
		for (Item item : stocksList) {
			this.stocksList.add(item);
		}
		
		for (Item item : cartList) {
			this.cartList.add(item);
		}
	}
	
	public ArrayList<Item> getStocksList() {
		return stocksList;
	}

	public void setStocksList(ArrayList<Item> stocksList) {
		this.stocksList = stocksList;
	}

	public ArrayList<Item> getCartList() {
		return cartList;
	}

	public void setCartList(ArrayList<Item> cartList) {
		this.cartList = cartList;
	}
	
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (this == obj) {
			return true;
		} else if(!(obj instanceof StoreStock)) {
			return false;
		}
		
		StoreStock storeStock = (StoreStock)obj;
		ArrayList<Item> sl = storeStock.getStocksList();
		
		if (sl.size() != stocksList.size()) {
			return false;
		}
		
		for (int i = 0; i < stocksList.size(); i++) {
			if (stocksList.get(i) == null && sl.get(i) != null) {
				return false;
			} else if (stocksList.get(i) != null && sl.get(i) == null) {
				return false;
			} else if (stocksList.get(i) != null && !stocksList.get(i).equals(sl.get(i))) {
				return false;
			}
		}
		
		ArrayList<Item> cl = storeStock.getCartList();
		
		if (cl.size() != cartList.size()) {
			return false;
		}
		
		for (int i = 0; i < cartList.size(); i++) {
			if (cartList.get(i) == null && cl.get(i) != null) {
				return false;
			} else if (cartList.get(i) != null && cl.get(i) == null) {
				return false;
			} else if (cartList.get(i) != null && !cartList.get(i).equals(cl.get(i))) {
				return false;
			}
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		String str = "stocks list:" + "\n";
		for (Item item : stocksList) {
			str += item.toString() + "\n";
		}
		
		str += "stocks list:" + "\n";
		for (Item item : cartList) {
			str += item.toString() + "\n";
		}
		
		return str;
	}
	
	// create shaved item with name, price
	public Item createItem(String name, double price) {
		return new ShelvedItem(name, price);
	}
	
	// create produce item with name, price and expire date
	public Item createItem(String name, double price, String expireDate) {
		return new ProduceItem(name, price, expireDate);
	}
	
	// create age restricted item with name, price and restrict age 
	public Item createItem(String name, double price, int restrictAge) {
		return new AgeRestrictedItem(name, price, restrictAge);
	}
	
	// add item to stocks list
	public void addItem(Item item) {
		stocksList.add(item);
	}
	
	// search items in stocks by name
	public ArrayList<Item> searchItem(String name) {
		ArrayList<Item> resultList = new ArrayList<Item>();
		for (Item item : stocksList) {
			if (name.equals(item.getName())) {
				resultList.add(item);
			}
		}
		
		return resultList;
	}
	
	// get the count in stocks of name
	public int getCountInStock(String name) {
		int count = 0;
		for (Item item : stocksList) {
			if (name.equals(item.getName())) {
				count++;
			}
		}
		
		return count;
	}
	
	// get the count in cart of name
	public int getCountInCart(String name) {
		int count = 0;
		for (Item item : cartList) {
			if (name.equals(item.getName())) {
				count++;
			}
		}
		
		return count;
	}
	
	// get the item type
	public String getItemType(String name) {
		String type = "";
		for (Item item : stocksList) {
			if (item.getName().equals(name)) {
				if (item.toString().contains("ShelvedItem")) {
					type = "ShelvedItem";
				} else if (item.toString().contains("ProduceItem")) {
					type = "ProduceItem";
				} else if (item.toString().contains("AgeRestrictedItem")) {
					type = "AgeRestrictedItem";
				}
				return type;
			}
		}
		
		return type;
	}
	
	// modify shelved item method with 3 parameters
	public void modifyItem(String name, String newName, double newPrice) {
		for (Item item : stocksList) {
			if (item.getName().equals(name)) {
				item.setName(newName);
				item.setPrice(newPrice);
			}
		}
	}
	
	// modify produce item method with 4 parameters
	public void modifyItem(String name, String newName, double newPrice, String newDate) {
		for (Item item : stocksList) {
			if (item.getName().equals(name)) {
				ProduceItem prdItm = (ProduceItem)item;
				prdItm.setName(newName);
				prdItm.setPrice(newPrice);
				prdItm.setDate(newDate);	
			}
		}
	}
	
	// modify age restricted item method with 4 parameters
	public void modifyItem(String name, String newName, double price, int newRestriceAge) {
		for (Item item : stocksList) {
			if (item.getName().equals(name)) {
				AgeRestrictedItem ageResItm = (AgeRestrictedItem)item;
				ageResItm.setName(newName);
				ageResItm.setPrice(price);
				ageResItm.setRestrictAge(newRestriceAge);
			}
		}
	}
	
	// add item to the cart
	public boolean addToCart(String name, int count) {
		ArrayList<Item> items = searchItem(name);
		if (items.size() <= 0) {
			System.out.println("Item name: " + name + "is invalid!");
			return false;
		}
		
		if (count < 1) {
			System.out.println("Item count is invalid!");
			return false;
		}
		
		if (count + getCountInCart(name) > getCountInStock(name)) {
			System.out.println("Stock is not enough! count: " + count + ", cart: " +
					getCountInCart(name) + ", stock: " + getCountInStock(name));
			return false;
		}
		
		for (int i = 0; i < count; i++) {
			cartList.add(items.get(0));
		}
		
		System.out.println("Add to cart success...");
		return true;
	}
	
	// check out
	public boolean checkout(double dollar) {
		double totalPrice = calculatePrice();
		if (dollar < totalPrice) {
			System.out.println("Check out failed! Insufficient money! Total price: $"
					+ totalPrice + ", Money: $" + dollar);
			return false;
		}
		
		for (Item item : cartList) {
			// remove items from store stock
			stocksList.remove(stocksList.indexOf(item));
		}
		
		cartList.clear();  // clear the cart
		double balance = dollar - totalPrice;  // calculate balance
		System.out.println("Check out success, your balance: $" + balance);
		return true;
	}
	
	// calculate the total price of items in the cart
	public double calculatePrice() {
		double price = 0.0;
		for (Item item : cartList) {
			price += item.getPrice();
		}
		
		return price;
	}
	
	// remove items from stocks list
	public boolean removeItems(String name, int count) {
		ArrayList<Item> items = searchItem(name);
		if (items.size() <= 0) {
			System.out.println("Item name: " + name + "is invalid!");
			return false;
		}
		
		if (count < 1) {
			System.out.println("Item count is invalid!");
			return false;
		}
		
		if (count > getCountInStock(name)) {
			System.out.println("Stock is not enough! count: " + count + ", stock: " + getCountInStock(name));
			return false;
		}
		
		for (Item item : items) {
			stocksList.remove(stocksList.indexOf(item));
			count--;
			if (count <= 0) {
				break;
			}
		}
		
		return true;
	}
	
	// get items stock, return a hash map with item name and stock count
	public HashMap<String, Integer> getItemsStockMap() {
		HashMap<String, Integer> stockMap = new HashMap<String, Integer>();
		// Initial stock map with name(key) and default count(value) 0
		for(Item item : stocksList) {
			stockMap.put(item.getName(), 0);
		}
		
		// calculate the count(value) of every name(key)
		for (String k : stockMap.keySet()) {
			int count = 0;
			for (Item item : stocksList) {
				if (item.getName().equals(k)) {
					count++;
				}
			}
			stockMap.put(k, count);
		}
		return stockMap;
	}
 
}
