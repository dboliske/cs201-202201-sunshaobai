/** 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 17, 2022
 * Section: section 3
 * Final Project:
 * A program that models a general store and allows users to add stock to the store,
 * search for available stock, and sell items.
 * Class name: Item
 * Description: The base class of all kinds of items.
 */

package project;

public abstract class Item {

	 protected String name;
	 protected double price;
	 
	 public Item() {
		 name = "item";
		 price = 0.0;
	 }
	 
	 public Item(String name, double price) {
		 this.name = name;
		 this.price = 0.0;  // Set a default value
		 setPrice(price);
	 }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		if (price >= 0.0) {
			this.price = price;
		}
	}
	 
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (this == obj) {
			return true;
		} else if(!(obj instanceof Item)) {
			return false;
		}
		
		Item item = (Item)obj;
		if (!this.name.equals(item.getName())) {
			return false;
		} else if (this.price != item.getPrice()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return "Item: Name is " + name + ", price is $" + price;
	}
	
	// Method to generate csv format of name and price
	public String toCSV() {
		return name + "," + price;
	}
}
