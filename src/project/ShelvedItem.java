/** 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 17, 2022
 * Section: section 3
 * Final Project:
 * A program that models a general store and allows users to add stock to the store,
 * search for available stock, and sell items.
 * Class name: ShelvedItem
 * Description: This class is the sub class of Item, which encapsulates all the properties and method of Shelved Item.
 */

package project;

public class ShelvedItem extends Item {
	
	public ShelvedItem() {
		super();
	}
	
	public ShelvedItem(String name, double price) {
		super(name, price);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof ShelvedItem)) {
			return false;
		}
		
		return true;
	}
	
	// Method to generate csv format of name and price
	public String toCSV() {
		return name + "," + price;
	}
	@Override
	public String toString() {
		return "ShelvedItem: Name is " + name + ", price is $" + price;
	}

}
