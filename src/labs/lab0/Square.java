// Name(Author): Shaobai Sun, 
// Course: CS201 Accelerated Intro to Computer Science
// Date: Jan. 24, 2022
// Program: Print a 10*10 square composed of character '$'
// Pseudocode:
// Outer loop: print Inner loop contents 10 times and form a square
// for (index from 0 to 9) {  
//      Inner loop: print 10 character 'S' in 1 line;
//    for (index from 0 to 9) {
//      print('$');
//    }
// }

package labs.lab0;

public class Square {
	// Use for loops to output, the character is '$'
	public static void main(String[] args) {
		// Output 10 columns
        for(int column = 0; column < 10; column++) {
        	// Output 10 rows
        	for(int row = 0; row < 10; row++) {
        		System.out.print("$  ");
        	}
        	// Start a new line
            System.out.println();
        }
	}
}