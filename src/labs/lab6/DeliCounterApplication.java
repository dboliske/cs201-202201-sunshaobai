/** 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Mar. 20, 2022
 * Section: section 3
 * Exercise
 * In this assignment, you will create an application that handles a queue at a deli counter.
 * You will allow customers to join the queue and deli workers to remove customers from the queue.
 * Solution
 * The solution is to create a single application class that presents a repeating menu with the following 3 options:
 * 1. Add customer to queue
 * 2. Help customer
 * 3. Exit
 * This requires you to create an ArrayList to be passed around to the various methods needed for the menu options.
 * What those methods are and how your class is laid out, is entirely up to you, however, it should be well designed.
 * To be well designed, the class should not be one single, massive main method. Minimally,
 * this means having separate methods for each menu option, but feel free to add more.
 * Add Customer to Queue
 * This menu option should prompt the user for a name (String) to be added to the end of an ArrayList
 * of customers and returns the customers position in the queue.
 * Help customer
 * This should remove the customer from the front of the queue (position 0) and prints out that customers name.
 * Exit
 * This exits the program.
 */

package labs.lab6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class DeliCounterApplication {
	
	public static void menu(ArrayList<String> queue, Scanner input) {
		boolean done = false;
		ArrayList<String> options = new ArrayList<String>(Arrays.asList("Add customer to queue", "Help customer", "Exit"));
		
		do {
			for(int i = 0; i < options.size(); i++) {  // Print all prompt options in this loop
				System.out.println((i + 1) + ". " + options.get(i));
			}
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1":  // Add customer to queue
					int postion = addCustomerToQueue(input, queue);
					System.out.println(" was added to postion " + postion + " success...");
					break;
				case "2":  // Help customer
					helpCustomer(queue);
					break;
				case "3":  // Exit
					done = true;
					exit(queue);
					break;
				default:
					System.out.println("Invalid input, please input angain!");
			}
		} while (!done);
	}
	
	public static int addCustomerToQueue(Scanner input, ArrayList<String> queue) {
		System.out.println("Please enter the name of customer to be added:");
		String name = input.nextLine();
		queue.add(name);
		System.out.print(name);  // print the added customer's name
		return queue.size();  // return the customer's position in the queue
	}
	
	public static void helpCustomer(ArrayList<String> queue) {
		if (queue.size() > 0) {
			String name = queue.remove(0);
			System.out.println(name + " was remove!");  // print the removed customer's name
		} else {
			System.out.println("Error: No customer to help!");
		}
	}
	
	public static void exit(ArrayList<String> queue) {
		System.out.println("Customers queue: " + queue.toString());  // print all customers before exit
		System.out.println("Goodbye!");
	}

	public static void main(String[] args) {
		// initialize the customers queue with 3 customers: [Tom, Steven, Jack]
		ArrayList<String> queue = new ArrayList<String>(Arrays.asList("Tom", "Steven", "Jack"));
		System.out.println("Customers queue: " + queue.toString());
		Scanner input = new Scanner(System.in);
		menu(queue, input);
		input.close();
	}
}
