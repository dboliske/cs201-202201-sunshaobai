/** 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 3, 2022
 * Section: section 3
 * Exercise:
 * Write a Java program that will implement the Bubble Sort algorithm as a method and sort the following array of integers:
 * {10, 4, 7, 3, 8, 6, 1, 2, 5, 9} 
 */

package labs.lab7;

public class BubbleSort {

	// Bubble sort algorithm
	public static int[] sort(int[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = 0; j < arr.length - 1 - i; j++) {
				if (arr[j + 1] < arr[j]) {
					int temp = arr[j + 1];
					arr[j + 1] = arr[j];
					arr[j] = temp;
				}
			}
		}
		return arr;
	}


	public static void main(String[] args) {
		int[] arr = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9}; 
		arr = sort(arr);
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}

}
