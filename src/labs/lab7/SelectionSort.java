/** 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 3, 2022
 * Section: section 3
 * Exercise:
 * Write a Java program that will implement the Selection Sort algorithm as a method and sorts the following array of doubles:
 * {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282} 
 */

package labs.lab7;

public class SelectionSort {
	
	// Selection Sort algorithm
	public static double[] sort(double[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			int minIndex = i;
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[j] <arr[minIndex]) {
					minIndex = j;
				}
			}
			
			if (minIndex != i) {
				double temp = arr[i];
				arr[i] = arr[minIndex];
				arr[minIndex] = temp;
			}
		}
		
		return arr;
	}

	public static void main(String[] args) {
		double[] arr = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
        arr = sort(arr);
        for (int i = 0; i < arr.length; i++) {
        	System.out.print(arr[i] + " ");
        }
	}

}
