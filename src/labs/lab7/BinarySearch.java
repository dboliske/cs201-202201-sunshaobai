/** 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 3, 2022
 * Section: section 3
 * Exercise:
 * Write a Java program that will implement the Binary Search algorithm as a recursive method
 * and be able to search the following arr of Strings for a specific value, input by the user:
 * {"c", "html", "java", "python", "ruby", "scala"}
 */

package labs.lab7;

import java.util.Scanner;

public class BinarySearch {
	
	public static int search(String[] arr, String value) {
		return search(arr, 0, arr.length - 1, value);
	}
	
	// Binary Search algorithm as a recursive method
	public static int search(String[] arr, int start, int end, String value) {
		if (start > end) {  // not found
			return -1;
		}
		
		int mid = (start + end) / 2;
		if (arr[mid].equals(value)) {
			return mid;
		} else if (arr[mid].compareTo(value) < 0) {
	    	return search(arr, mid + 1, end, value);
	    } else {
	    	return search(arr, start, mid - 1, value);
	    }
	}

	public static void main(String[] args) {
		String[] langArr = {"c", "html", "java", "python", "ruby", "scala"};
		Scanner input = new Scanner(System.in);
		System.out.print("Please input the program language you like: ");
		String lang = input.nextLine();
		int pos = search(langArr, lang);
		if (pos >= 0) {
			System.out.println(lang + " is found at position " + pos + ".");
		} else {
			System.out.println("I'm sorry, " + lang + " is not found!");
		}
		
        input.close();
	}

}
