/** 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Apr. 3, 2022
 * Section: section 3
 * Exercise:
 * Write a Java program that will implement the Insertion Sort algorithm as a method and sort the following array of Strings:
 * {"cat", "fat", "dog", "apple", "bat", "egg"} 
 */

package labs.lab7;

public class InsertionSort {

	// Insertion Sort algorithm
	public static String[] sort(String[] arr) {
		for (int i = 1; i < arr.length; i++) {
			for (int j = i; j > 0; j--) {
				if (arr[j].compareTo(arr[j - 1]) < 0) {
					String temp = arr[j];
					arr[j] = arr[j - 1];
					arr[j - 1] = temp;
				}
			}
		}
		
		return arr;
	}
	
	public static void main(String[] args) {
		String[] arr = {"cat", "fat", "dog", "apple", "bat", "egg"};
		arr = sort(arr);
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}

}
