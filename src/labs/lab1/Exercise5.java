// Author: Shaobai Sun, 
// Course: CS201 Accelerated Introduction to Computer Science
// Date: Feb. 7, 2022
// Exercise: Write a program that will do the following:
// Prompt the user for the length, width, and depth in inches of a box.
// Calculate the amount of wood (square feet) needed to make the box.
// Display the result to the screen with a descriptive message.
// Test your program with various inputs. Use a test table like above with as many rows as needed to test your program.
package labs.lab1;

import java.util.Scanner;

public class Exercise5 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);  // create a scanner
		
		// prompt for length
		System.out.print("Please enter the length in inches of a box: ");
		double length = Double.parseDouble(input.nextLine());
		
		// prompt for width
		System.out.print("Please enter the width in inches of a box: ");
		double width = Double.parseDouble(input.nextLine());

		// prompt for depth
		System.out.print("Please enter the depth in inches of a box: ");
		double depth = Double.parseDouble(input.nextLine());
		
		// Calculate the amount of wood (square feet) needed to make the box
		double surface_area = 2 * (length * width + length * depth + width * depth);
		System.out.println("The amount of wood (square feet) needed to make the box: " + surface_area);
		
		input.close();
	}

}
