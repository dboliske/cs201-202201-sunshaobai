// Author: Shaobai Sun, 
// Course: CS201 Accelerated Introduction to Computer Science
// Date: Feb. 7, 2022
// Exercise: Write a program that will do the following:
// Prompt the user for a temperature in Fahrenheit, convert the Fahrenheit to Celsius and display the result.
// Prompt the user for a temperature in Celsius, convert the Celsius to Fahrenheit and display the result.
// Test your program with various temperatures: low, high, middle. Use a test table with as many rows as needed to test your program. 
// Are you satisfied that your program works as expected? Submit your test plan and its results.

package labs.lab1;

import java.util.Scanner;

public class Exercise4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);  // create a scanner
		
		// prompt for a temperature in Fahrenheit
		System.out.print("Please enter a temperature in Fahrenheit: ");
		double temp_fah = Double.parseDouble(input.nextLine());
		double temp_cel = (temp_fah - 32.0) / 1.8;  // convert the Fahrenheit to Celsius
		// display the result
		System.out.println("temperature in Celsius: " + temp_cel + "��C");

		// prompt for a temperature in Celsius
		System.out.print("Please enter a temperature in Celsius: ");
		temp_cel = Double.parseDouble(input.nextLine());
		temp_fah = (temp_cel * 1.8) + 32; // convert the Celsius to Fahrenheit
		// display the result
		System.out.println("temperature in Fahrenheit: " + temp_fah + "��F");
		
		input.close();
	}

}
