// Author: Shaobai Sun, 
// Course: CS201 Accelerated Introduction to Computer Science
// Date: Feb. 7, 2022
// Exercise: Write a program that will convert inches to centimeters:
// Prompt the user for inches.
// Convert inches to centimeters
// Display the result to the console.
// Test your program with various inputs. Use a test table like above with as many rows as needed to test your program.

package labs.lab1;

import java.util.Scanner;

public class Exercise6 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);  // create a scanner
		
		// prompt user for inches
		System.out.print("Please enter the inches: ");
		double inches = Double.parseDouble(input.nextLine());
		double centimeters = inches * 2.54;  // Convert inches to centimeters
		System.out.println("centimeters: " + centimeters);
		
		input.close();
	}

}
