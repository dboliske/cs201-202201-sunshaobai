// Author: Shaobai Sun, 
// Course: CS201 Accelerated Introduction to Computer Science
// Date: Feb. 7, 2022
// Exercise: Getting a char from keyboard input: Prompt a user for a first name; 
// display the user's first initial to the screen. (Hint: Use the String method charAt(0)).

package labs.lab1;

import java.util.Scanner;

public class Exercise3 {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		// prompt the user for a first name
		System.out.print("Please enter your first name: ");
		
		// read in the first initial character
		char init_char = input.nextLine().charAt(0);
		
		// display the user's first initial to the screen
		System.out.println("Your first initial: " + init_char);
		
		input.close();
	}
}
