// Author: Introduction Sun, 
// Course: CS201 Accelerated Intro to Computer Science
// Date: Feb. 7, 2022
// Exercise: Reading input and writing output: Writing a Java program that will prompt a user for a name; 
// save the input and echo the name to the console.

package labs.lab1;

import java.util.Scanner;

public class Exercise1 {
	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		// prompt the user for a name
		System.out.print("Please input your name: ");
		String name = input.nextLine();  // Save the input
		input.close();
		
		System.out.println("Echo: " + name);  // echo the name to the console
	}
}
