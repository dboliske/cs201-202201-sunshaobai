// Author: Shaobai Sun, 
// Course: CS201 Accelerated Introduction to Computer Science
// Date: Feb. 7, 2022
// Exercise: Performing arithmetic calculations: Output the result of the following calculations; 
// be sure to write a descriptive comment for each output so the reader knows what is being calculated:
// Your age subtracted from your father's age
// Your birth year multiplied by 2
// Convert your height in inches to cms
// Convert your height in inches to feet and inches where inches is an integer (mode operator)

package labs.lab1;

public class Exercise2 {

	public static void main(String[] args) {
		int my_age = 34;
		int my_father_age = 60;
		int age_difference = my_father_age - my_age;
		// My age subtracted from my father's age
		System.out.println(age_difference);
		
		int my_birth_year = 1988;
		// My birth year multiplied by 2
		System.out.println(my_birth_year * 2);
		
		int my_height_inches = 67;
		double my_height_cms = (double)my_height_inches / 0.3937008;
		// Convert my height in inches to cms
		System.out.println(my_height_cms);
		
		double my_height_feet = (double)my_height_inches * 0.0833333;
		// Convert my height in inches to feet and inches where inches is an integer
		System.out.println(my_height_feet);

	}

}
