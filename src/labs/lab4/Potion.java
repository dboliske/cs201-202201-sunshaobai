// Author: Shaobai Sun, 
// Course: CS201 Accelerated Introduction to Computer Science
// Date: Feb. 20, 2022
// Section: section 3
// Part III: Potion Class
// Write the class, Potion.java following this UML Diagram and doing the following:
// 1. Create two instance variables, name (a String) and strength (a double).
// 2. Write the default constructor.
// 3. Write the non-default constructor.
// 4. Write two accessor methods, one for each instance variable.
// 5. Write two mutator methods, one for each instance variable.
// 6. Write a method that will return the entire as a single string (the toString method).
// 7. Write a method that will return true if the strength is between 0 and 10.
// 8. Write a method that will compare this instance to another Potion (the equals method).


package labs.lab4;

public class Potion {
	
	// Create two instance variables, name (a String) and strength (a double)
	private String name;
	private double strength;
	
	// default constructor
	public Potion() {
		name = "***";
		strength = 0.0;
	}
	
	// non-default constructor
	public Potion(String name, double strength) {
		this.name = name;
		if (validStrength(strength)) {
			this.strength = strength;
		} else {
			this.strength = 0.0;  // set to default
		}
	}
	
	// accessor methods for name
	public String getName() {
		return name;
	}
	
	// accessor methods for strength
	public double getStrength() {
		return strength;
	}
	
	// mutator methods for name
	public void setName(String name) {
		this.name = name;
	}
	
	// mutator methods for strength
	public void setStrength(double strength) {
		if (validStrength(strength)) {
			this.strength = strength;
		}
	}
	
	// toString method
	public String toString() {
		return "[" + name + ": " + strength + "]";
	}
	
	// strength valid method
	public boolean validStrength(double strength) {
		if (strength >= 0.0 && strength <= 10.0) {
			return true;
		}
		
		return false;
	}
	
	// equals method
	public boolean equals(Potion potion) {
		if (this.name.equals(potion.getName()) &&
			this.strength == potion.getStrength()) {
			return true;
		}
		
		return false;
	}

}
