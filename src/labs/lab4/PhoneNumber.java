// Author: Shaobai Sun, 
// Course: CS201 Accelerated Introduction to Computer Science
// Date: Feb. 20, 2022
// Section: section 3
// Part II: PhoneNumber Class
// Write the class, PhoneNumber.java following this UML Diagram and doing the following:
// 1. Create three instance variables, countryCode, areaCode and number, all of which should be Strings.
// 2. Write the default constructor.
// 3. Write the non-default constructor.
// 4. Write 3 accessor methods, one for each instance variable.
// 5. Write 3 mutator methods, one for each instance variable.
// 6. Write a method that will return the entire phone number as a single string (the toString method).
// 7. Write a method that will return true if the areaCode is 3 characters long.
// 8. Write a method that will return true if the number is 7 characters long.
// 9. Write a method that will compare this instance to another PhoneNumber (the equals method).


package labs.lab4;

public class PhoneNumber {
	
	// Create three instance variables, countryCode, areaCode and number
	private String countryCode;
	private String areaCode;
	private String number;
	
	// default constructor
	public PhoneNumber() {
		countryCode = "00";
		areaCode = "000";
		number = "0000000";
	}
	
	// non-default constructor
	public PhoneNumber(String countryCode, String areaCode, String number) {
		this.countryCode = countryCode;
		
		if (validAreaCode(areaCode)) {
			this.areaCode = areaCode;
		} else {
			this.areaCode = "000";  // set to default
		}
		
		if (validNumber(number)) {
			this.number = number;
		} else {
			this.number = "0000000";  // set to default
		}
	}
	
	// accessor methods for countryCode
	public String getCountryCode() {
		return countryCode;
	}
	
	// accessor methods for areaCode
	public String getAreaCodee() {
		return areaCode;
	}
	
	// accessor methods for number
	public String getNumber() {
		return number;
	}
	
	// mutator method for countryCode
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	// mutator method for areaCode
	public void setAreaCode(String areaCode) {
		if (validAreaCode(areaCode)) {
			this.areaCode = areaCode;
		}
	}
	
	// mutator method for number
	public void setNumber(String number) {
		if (validNumber(number)) {
			this.number = number;
		}
	}
	
	// toString method
	public String toString() {
		// return a phone number as a common style
		return "+" + countryCode + "-" + areaCode + "-" + number;
	}
	
	// areaCode valid method
	public boolean validAreaCode(String areaCode) {
		if (areaCode.length() == 3) {  // length is 3
			return true;
		}
		
		return false;
	}
	
	// number valid method
	public boolean validNumber(String number) {
		if (number.length() == 7) {  // length is 7
			return true;
		}
		
		return false;
	}

	// equals method
	public boolean equals(PhoneNumber phone_number) {
		if (countryCode.equals(phone_number.getCountryCode()) &&
			areaCode.equals(phone_number.getAreaCodee()) &&
			number.equals(phone_number.getNumber())) {
			return true;
		}
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
