// Author: Shaobai Sun, 
// Course: CS201 Accelerated Introduction to Computer Science
// Date: Feb. 20, 2022
// Section: section 3
// 10. Now write an application class that instantiates two instances of GeoLocation. 
//    One instance should use the default constructor and the other should use the non-default constructor. 
//    Display the values of the instance variables by calling the accessor methods.


package labs.lab4;

public class GeoLocationApplication {

	public static void main(String[] args) {
		// create instance by using default constructor
		GeoLocation gl1 = new  GeoLocation();
		// create instance by using non-default constructor
		GeoLocation gl2 = new  GeoLocation(121, 38);
		
		//  Display the values of the instance variables by calling the accessor methods
		System.out.println("gl1, lan: " + gl1.getLat() + ", lnt: " + gl1.getLng());
		System.out.println("gl2, lan: " + gl2.getLat() + ", lnt: " + gl2.getLng());
	}

}
