# Lab 4

## Total

21/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        5/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        5/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        5/8
  * Application Class   1/1
* Documentation         3/3

## Comments

~~Lab not submitted~~
None of the non-default constructors or mutator methods use their classes' validation methods to validate values before setting them to instance variables.

Additionally, the lab was submitted 4 days late. -40%

The student showed the proofs that the late submission was not his fault. I already checked it and added the loss point. +40%
