// Author: Shaobai Sun, 
// Course: CS201 Accelerated Introduction to Computer Science
// Date: Feb. 20, 2022
// Section: section 3
// 9. Now write an application class that instantiates two instances of Potion. One instance should use the default constructor and the other should use the non-default constructor.
//    Display the values of each object by calling the toString method.


package labs.lab4;

public class PotionApplication {

	public static void main(String[] args) {
		// create instance by using default constructor
		Potion potion1 = new Potion();
		// create instance by using default constructor
		Potion potion2 = new Potion("amoxicillin", 5.0);
		
		// Display the values of each object by calling the toString method.
		System.out.println("potion1: " + potion1.toString());
		System.out.println("potion2: " + potion2.toString());
	}

}
