// Author: Shaobai Sun, 
// Course: CS201 Accelerated Introduction to Computer Science
// Date: Feb. 20, 2022
// Section: section 3
// Part II: PhoneNumber Class
// Write the class, PhoneNumber.java following this UML Diagram and doing the following:
// 10. Now write an application class that instantiates two instances of PhoneNumber. One instance should use the default constructor and the other should use the non-default constructor.
//     Display the values of each object by calling the toString method.

package labs.lab4;

public class PhoneNumberApplication {

	public static void main(String[] args) {
		// create instance by using default constructor
		PhoneNumber phone_number1 = new PhoneNumber();
		// create instance by using non-default constructor
		PhoneNumber phone_number2 = new PhoneNumber("86", "021", "7654321");
		
		// Display the values of each object by calling the toString method
		System.out.println("phone_number1: " + phone_number1.toString());
		System.out.println("phone_number2: " + phone_number2.toString());
	}

}
