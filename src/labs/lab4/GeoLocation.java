// Author: Shaobai Sun, 
// Course: CS201 Accelerated Introduction to Computer Science
// Date: Feb. 20, 2022
// Section: section 3
// Part I: GeoLocation Class
// Write the class, GeoLocation.java following this UML Diagram and doing the following:
// 1. Create two instance variables, lat and lng, both of which should be doubles.
// 2. Write the default constructor.
// 3. Write the non-default constructor.
// 4. Write 2 accessor methods, one for each instance variable.
// 5. Write 2 mutator methods, one for each instance variable.
// 6. Write a method that will return the location in the format "(lat, lng)" (the toString method).
// 7. Write a method that will return true if the latitude is between -90 and +90.
// 8. Write a method that will return true if the longitude is between -180 and +180.
// 9. Write a method that will compare this instance to another GeoLocation (the equals method).


package labs.lab4;

public class GeoLocation {
	
	// Create two instance variables, lat and lng
	private double lat;
	private double lng;
	
	// default constructor
	public GeoLocation() {
		lat = 0.0;
		lng = 0.0;
	}
	
	// non-default constructor
	public GeoLocation(double lat, double lng) {
		if (validLat(lat)) {
			this.lat = lat;
		} else {
			this.lat = 0.0;  // set to default
		}
		
		if (validLng(lng)) {
			this.lng = lng;
		} else {
			this.lng = 0.0;  // set to default
		}
		
	}
    
	// accessor method for lat
	public double getLat() {
		return lat;
	}
	
	// accessor method for lng
	public double getLng() {
		return lng;
	}
	
    // mutator method for lat
	public void setLat(double lat) {
		if (validLat(lat)) {
			this.lat = lat;
		}
	}
	
	// mutator method for lng
	public void setLng(double lng) {
		if (validLng(lng)) {
			this.lng = lng;
		}
	}
	
	// toString method
	public String toString() {
		return "(" + lat + ", " + lng + ")";
	}
	
	// latitude valid method
	public boolean validLat(double lat) {
		if (lat >= -90 && lat <= 90) {
			return true;
		}
		
		return false;
	}
	
	// longitude valid method
	public boolean validLng(double lng) {
		if (lng >= -180 && lng <= 180) {
			return true;
		}
		
		return false;
	}
	
	// equals method
	public boolean equals(GeoLocation gl) {
		if (lat == gl.getLat() && lng == gl.getLng()) {
			return true;
		}
		
		return false;
	}
}
