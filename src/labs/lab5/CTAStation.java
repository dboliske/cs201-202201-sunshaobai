/** 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Mar. 6, 2022
 * Section: section 3
 * Create a class that will implement a CTAStation, which should inherit from GeoLocation, with the UML diagram below.
 */

package labs.lab5;

public class CTAStation extends GeoLocation {
	private String name;
	private String location;
	private boolean wheelChair;
	private boolean open;
	
	public CTAStation() {
		super();
		name = "Shanghai Yangpu Park";
		location = "elevated";
		wheelChair = false;
		open = false;
	}
	
	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
		super(lat, lng);
		this.name = name;
		this.location = location;
		this.wheelChair = wheelchair;
		this.open = open;
	}
	
	public String getName() {
		return name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public boolean hasWheelChair() {
		return wheelChair;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public void setWheelChair(boolean wheelchair) {
		this.wheelChair = wheelchair;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	@Override
	public String toString() {
		return "CTAStation name is " + name + ", lat: " + super.getLat() + ", lng: " + super.getLng() +
				", location: " + location + ", wheelChair: " + wheelChair + ", open: " + open;
	}
	
	// equals method
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof CTAStation)) {
			return false;
		}
		
		CTAStation cs = (CTAStation)obj;
		if (!this.name.equals(cs.getName())) {
			return false;
		} else if (!this.location.equals(cs.getLocation())) {
			return false;
		} else if (!this.wheelChair != cs.hasWheelChair()) {
			return false;
		} else if (!this.open != cs.isOpen()) {
			return false;
		}
		
		return true;
	}
	
	
	
	
	
	
	
}
