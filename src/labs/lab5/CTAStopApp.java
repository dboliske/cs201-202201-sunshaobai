/** 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Mar. 6, 2022
 * Section: section 3
 * CTAStopApp
 * This application class will display a menu of options that the user can select from to display stations that meet certain criteria. 
 * The UML diagram for the class is as shown below, but here is the general description for each method:
 * main:
 * Calls readFile to load the data
 * Passes that data to menu
 * readFile:
 * Reads stations from the input file and stores the data in an instance of CTAStation[].
 * menu:
 * Displays the menu options, which should be the following:
 * Display Station Names
 * Display Stations with/without Wheelchair access
 * Display Nearest Station
 * Exit
 * Performs the operation requested by the user
 * Loops until 'Exit' is chosen
 * displayStationNames:
 * Iterates through CTAStation[] and prints the names of the stations.
 * displayByWheelchair:
 * Prompts the user for accessibility ('y' or 'n')
 * Checks that the input is 'y' or 'n', continues to prompt the user for 'y' or 'n' until one has been entered
 * If char entered is a valid choice:
 * Determine which boolean to use ('y' -> true, 'n' -> false)
 * Loop through the CTAStation[] to look at wheelchair and display CTAStations that meet the requirement
 * Display a message if no stations are found
 * displayNearest:
 * Prompts the user for a latitude and longitude
 * Uses the values entered to iterate through the CTAStation[] to find the nearest station (using calcDistance) and displays it to the console
 */

package labs.lab5;

import java.io.File;
import java.util.Scanner;

public class CTAStopApp {
	
	// Reads stations from the input file and stores the data in an instance of CTAStation[].
	public static CTAStation[] readFile(String filename) {
		CTAStation[] stations = new CTAStation[5];  // set the size of array to 5, will resize it if needed
		int count = 0;  // data number
		int line_number = 0;  // line number
		
		try {
			File file = new File(filename);
			Scanner input = new Scanner(file);
			
			// end-of-file loop
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					// skip first line
					if (line_number > 0) {
						String[] values = line.split(",");
						CTAStation cs = new CTAStation(
										values[0],  // name
										Double.parseDouble(values[1]),  // lat, convert String to float
										Double.parseDouble(values[2]),  // Lng, convert String to float
										values[3],  // location
										Boolean.parseBoolean(values[4]),  // wheelchair, convert String to boolean
										Boolean.parseBoolean(values[5])   // open, convert String to boolean
								);
						
						if (stations.length == count) {  // need to increase array size
							stations = resize(stations, stations.length * 2);
						}
						stations[count] = cs;
						count ++;
					}
					line_number ++;
				} catch (Exception e) {
					System.out.println("Data format ERROR!");
				}
				
			}
			
			input.close();
		} catch (Exception e) {
			System.out.println("Reading file ERROR!");
		}
		
		// Trim array to exact size
		stations = resize(stations, count);
		return stations;
	}
	
	// Displays the menu options
	public static void menu(Scanner input, CTAStation[] stations) {
		boolean done = false;
		
		do {
			System.out.println("1. Display Station Names");
			System.out.println("2. Display Stations with/without Wheelchair access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1":  // Print names
					displayStationNames(stations);
					break;
				case "2":  // Stations with/without Wheelchair access
					displayByWheelchair(input, stations);
					break;
				case "3": // Display Nearest Station
					displayNearest(input, stations);
					break;
				case "4": // Exit
					done = true;
					break;
				default:
					System.out.println("I'm sorry, your input is invalid, Please input again!");
			}
		} while (!done);
	}
	
	// Iterates through CTAStation[] and prints the names of the stations.
	public static void displayStationNames(CTAStation[] stations) {
		System.out.println("Names of the station as below:");
		for (int i = 0; i < stations.length; i++) {
			System.out.println(stations[i].getName());
		}
	}
	
	// display Stations with/without Wheelchair access
	public static void displayByWheelchair(Scanner input, CTAStation[] stations) {
		boolean done = false;
		boolean result = false;
		do {
			System.out.println("y: Display Stations with Wheelchair access");
			System.out.println("n: Display Stations without Wheelchair access");
			System.out.print("Your choice: ");
			String choice = input.nextLine();
			switch (choice.toLowerCase()) {
				case "y":
				case "yes":
					result = true;
					done = true;
					break;
				case "n":
				case "no":
					result = false;
					done = true;
					break;
				default:
					System.out.println("Your input is not yes or no, please input again!");
					break;
			}
		} while (!done);
		
		for (int i = 0; i < stations.length; i++) {
			if (result) {
				if (stations[i].hasWheelChair()) {  // stations with wheel chair
					System.out.println(stations[i]);
				}
			} else {
				if (!stations[i].hasWheelChair()) {  // stations without wheel chair
					System.out.println(stations[i]);
				}
			}
		}
	}
	
	// Find the nearest station and print it to console
	public static void displayNearest(Scanner input, CTAStation[] stations) {
		double lat = 0.0;
		double lng = 0.0;
		boolean done = false;
		do {
			System.out.println("latitude:");
			try {
				String lat_str = input.nextLine();
				lat = Double.parseDouble(lat_str);
				done = true;
			} catch (Exception e) {
				System.out.println("Invalid input, please input a number!");
			}
		} while (!done);
		
		done = false;
		do {
			System.out.println("longitude:");
			try {
				String lng_str = input.nextLine();
				lng = Double.parseDouble(lng_str);
				done = true;
			} catch (Exception e) {
				System.out.println("Invalid input, please input a number!");
			}
		} while (!done);
		
		// array to store the distances
		double distances[] = new double[stations.length]; 
		for (int i = 1; i < stations.length; i++) {
			distances[i] = stations[i].calcDistance(lat, lng);
		}
		
		int m = 0;  // store index of the nearest station
		for (int i = 1; i < distances.length; i++) {
			if (distances[i] < distances[m]) {
				m = i;
			}
		}
		
		// print nearest station
		System.out.println(stations[m]);
	}
	
	public static CTAStation[] resize(CTAStation[] data, int size) {
		CTAStation[] temp = new CTAStation[size];
		int limit = data.length > size ? size : data.length;
		for (int i = 0; i < limit; i++) {
			temp[i] = data[i];
		}
		
		return temp;
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		CTAStation[] stations = readFile("src/labs/lab5/CTAStops.csv");
		menu(input, stations);
		input.close();
		System.out.println("Goodbye...");
	}

}
