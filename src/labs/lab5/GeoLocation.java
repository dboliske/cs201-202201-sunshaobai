/** 
 * Author: Shaobai Sun, 
 * Course: CS201 Accelerated Introduction to Computer Science
 * Date: Mar. 6, 2022
 * Section: section 3
 * Exercise
 * In this assignment, you will write a user-defined class that will encapsulate a CTAStation.
 * You will then use arrays to store CTAStation data.
 * Solution:
 * GeoLocation
 * Add to your GeoLocation class from last week to include the following:
 * A method called calcDistance that takes another GeoLocation and returns a double.
 * Note: This should use the formula Math.sqrt(Math.pow(lat1 - lat2, 2) + Math.pow(lng1 - lng2, 2)).
 * A method called calcDistance that takes a lng and lat and returns a double. Note: See above formula.
 */

package labs.lab5;

public class GeoLocation {
	
	// Create two instance variables, lat and lng
	private double lat;
	private double lng;
	
	// default constructor
	public GeoLocation() {
		lat = 0.0;
		lng = 0.0;
	}
	
	// non-default constructor
	public GeoLocation(double lat, double lng) {
		this.lat = lat;
		this.lng = lng;
	}
    
	// accessor method for lat
	public double getLat() {
		return lat;
	}
	
	// accessor method for lng
	public double getLng() {
		return lng;
	}
	
    // mutator method for lat
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	// mutator method for lng
	public void setLng(double lng) {
		this.lng = lng;
	}
	
	// toString method
	public String toString() {
		return "(" + lat + ", " + lng + ")";
	}
	
	// latitude valid method
	public boolean validLat() {
		if (lat >= -90 && lat <= 90) {
			return true;
		}
		
		return false;
	}
	
	// longitude valid method
	public boolean validLng() {
		if (lng >= -180 && lng <= 180) {
			return true;
		}
		
		return false;
	}
	
	// equals method
	public boolean equals(GeoLocation gl) {
		if (lat == gl.getLat() && lng == gl.getLng()) {
			return true;
		}
		
		return false;
	}
	
	// method to calculate distance
	public double calcDistance (GeoLocation g) {
		return Math.sqrt(Math.pow(lat - g.getLat(), 2) + Math.pow(lng - g.getLng(), 2));
	}
	
	// method to calculate distance
	public double calcDistance (double lat, double lng) {
		return Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng -lng, 2));
	}

}
