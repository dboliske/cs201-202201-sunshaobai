// Author: Shaobai Sun, 
// Course: CS201 Accelerated Introduction to Computer Science
// Date: Feb. 13, 2022
// Exercise: Write a program that will continue to prompt the user for numbers, storing them in an array,
// until they enter "Done" to finish, then prompts the user for a file name so that these values can be saved to that file. 
// For example, if the user enters "output.txt", then the program should write the numbers that have been read to "output.txt".


package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String line = "";
		String file_name = "";
		boolean done = false;
		double[] numbers = new double[2];
		int count = 0;
		do {
			System.out.print("Please input numbers: ");
			line = input.nextLine();
			if(line.equals("Done")) {
				System.out.print("Please input file name to save numbers: ");
				file_name = input.nextLine();
				done = true;
			}
			
			if (count == numbers.length) {
				// resize the array
				double[] bigger = new double[2 * numbers.length];
				for (int i=0; i<numbers.length; i++) {
					bigger[i] = numbers[i];
				}
				numbers = bigger;
				bigger = null;
			}
			
			try {
				if (!done) {  // if done, not pare line again to avoid exception
					numbers[count] = Double.parseDouble(line);
					count++;
				}
			} catch (Exception e) {
				System.out.println("Input Error: '" + line + "' is not a valid number!");
			}
			
		} while (!done);
		
		input.close();
		
		// trim array down
		double[] smaller = new double[count];
		for (int i=0; i<smaller.length; i++) {
			smaller[i] = numbers[i];
		}
		numbers = smaller;
		smaller = null;
		
		try {
			// create a FileWriter to store number to path: src/labs/lab3/
			FileWriter f = new FileWriter("src/labs/lab3/" + file_name);
			
			for (int i=0; i<numbers.length; i++) {
				f.write(numbers[i] + "\n");
			}
			f.flush();
			f.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println("Finished Writing");

	}

}
