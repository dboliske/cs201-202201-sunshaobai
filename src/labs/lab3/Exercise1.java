// Author: Shaobai Sun, 
// Course: CS201 Accelerated Introduction to Computer Science
// Date: Feb. 13, 2022
// Exercise: You have been given a file called "src/labs/lab3/grades.csv". It contains a list of students and their exam grades. 
// Write a program that reads in the file and computes the average grade for the class and then prints it to the console.

package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) throws IOException {
		File f = new File("src/labs/lab3/grades.csv");  // create a file object
		Scanner input = new Scanner(f);   // scanner for file
		
		int[] grades = new int[14];  // array to store the grades
		int count = 0;
		while (input.hasNextLine()) {
			String line = input.nextLine();
			String[] values = line.split(","); // {"Hermina Azhar", "92"}
			grades[count] = Integer.parseInt(values[1]);
			count++;
		}
		
		input.close();
		
		int total = 0;
		for (int i=0; i<grades.length; i++) {
			total += grades[i];
		}
		double average = total / grades.length;  // calculate the average grade
		System.out.println(" average grade of the class: " + average);
	}

}
