// Author: Introduction Sun, 
// Course: CS201 Accelerated Introduction to Computer Science
// Date: Feb. 7, 2022
// Exercise: Write a Java program that will repeatedly display a menu of choices to a user and prompt them to enter an option. 
// You should use the following options:
//	1.Say Hello - This should print "Hello" to console.
//	2.Addition - This should prompt the user to enter 2 numbers and return the sum of the two.
//	3.Multiplication - This should prompt the user to enter 2 numbers and return the product of the two.
//	4.Exit - Leave the program
// The program should continue to return to the menu until the user enters 4.

package labs.lab2;

import java.util.Scanner;

public class Exercise3 {

	public static void main(String[] args) {
		// create a scanner to read user input
		Scanner input = new Scanner(System.in); 
		
		boolean done = false; // flag control variable
		do {
			System.out.println("===============");
			System.out.println("1.Say Hello");
			System.out.println("2.Addition");
			System.out.println("3.Multiplication");
			System.out.println("4.Exit");
			System.out.println("Your choice:");
			String choice = input.nextLine();  // get user's choice
			
			double number1 = 0.0;
			double number2 = 0.0;
			switch (choice) {
				case "1":
					System.out.println("Hello");
					break;
				case "2":
					System.out.println("Please enter 2 numbers:");
					System.out.println("numbers 1:");
					number1 = Double.parseDouble(input.nextLine());
					System.out.println("numbers 2:");
					number2 = Double.parseDouble(input.nextLine());
					double sum = number1 + number2;  // calculate the sum
					System.out.println("The sum of the two is: " + sum);
					break;
				case "3":
					System.out.println("Please enter 2 numbers:");
					System.out.println("numbers 1:");
					number1 = Double.parseDouble(input.nextLine());
					System.out.println("numbers 2:");
					number2 = Double.parseDouble(input.nextLine());
					double product = number1 * number2;   // calculate the product
					System.out.println("The product of the two is: " + product);
					break;
				case "4":
					done = true;
					break;
				default:
					System.out.println("Your choice is invalid, please input again!");
			}
		} while (!done);
		
		input.close();
		
		System.out.println("Goodbye!");
	}

}
