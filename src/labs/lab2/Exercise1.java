// Author: Introduction Sun, 
// Course: CS201 Accelerated Intro to Computer Science
// Date: Feb. 7, 2022
// Exercise: Write a Java program that will prompt the user for a number and print out a square with those dimensions. 
// For example, if the user enters 5, return the following:
//    * * * * *
//    * * * * *
//    * * * * *
//    * * * * *
//    * * * * *

package labs.lab2;

import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		// prompt the user for a number
		System.out.print("number: ");
		int number = Integer.parseInt(input.nextLine());
		
		input.close();
		
		for (int column = 0; column < number; column++) {
			for (int row = 0; row < number; row++) {
				System.out.print("* ");
			}
			System.out.println();   // Start a new line
		}

	}

}
