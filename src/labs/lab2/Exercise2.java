// Author: Introduction Sun, 
// Course: CS201 Accelerated Intro to Computer Science
// Date: Feb. 7, 2022
// Exercise: Write a Java program that will prompt the user for the grades for an exam, computes the average, 
// and returns the result. Your program must be able to handle an unspecified number of grades.
// Be sure to test your code with a variety of values and number of grades.
// (Hint: Tell the user to enter -1 when they finished entering grades)

package labs.lab2;

import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		int total_grades = 0;
		int count = 0;
		boolean done = false; // flag control variable
		do {
			// prompt the user for the grades for an exam
			System.out.println("Please input your grades of the exam(0~100): ");
			System.out.println("-1: when you finished entering grades");
			int grade = Integer.parseInt(input.nextLine());
			
			if (grade == -1) {
				System.out.println("finished entering grades");
				double average = total_grades / count;
				System.out.println("the average is: " + average);
				done = true;
			} else if (grade > 100 || grade < -1) {
				System.out.println("Invalid input! Goodbye~");
				done = true;
			} else {
				total_grades += grade;
				count++;
			}
			
		} while (!done);
		
		input.close();
	}

}
